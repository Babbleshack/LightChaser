
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sunspotworld;

import com.sun.spot.resources.Resources;
import com.sun.spot.resources.transducers.IToneGenerator;
import com.sun.spot.resources.transducers.ITriColorLED;
import com.sun.spot.resources.transducers.ITriColorLEDArray;
import com.sun.spot.sensorboard.peripheral.ToneGenerator;
import com.sun.spot.service.BootloaderListenerService;
import com.sun.spot.util.Utils;
import javax.microedition.midlet.MIDlet;
import javax.microedition.midlet.MIDletStateChangeException;


/**
 *
 * @author babbleshack
 */
public class LightsChaser extends MIDlet 
{
    //get LED array
    private ITriColorLEDArray leds =
            (ITriColorLEDArray) Resources.lookup(ITriColorLEDArray.class);
    private IToneGenerator toneGen = 
            (IToneGenerator) Resources.lookup(ToneGenerator.class);

    protected void startApp() throws MIDletStateChangeException
    {
        //checks if the usb port has a aspot connected then listens
        //for commands from the host computer.
        BootloaderListenerService.getInstance().start();
        int i = 0;
        while(i<9)
        {
            //calls method to turn eds red one at a time (left-right)
            setLEDsOnLeft(leds,"red",300.0);
            //calls ethod to turn leds green (right-left)
            setLEDsOnRight(leds, "green", 900.0);
            //calls methods to turn leds blue, (left-right)
            setLEDsOnLeft(leds, "blue", 2000.0);
            
            setLEDsOFFLeft(leds);
            
            i++;
            toneGen.startTone(120, 30);
        }
        
        //MIDLet exits
        notifyDestroyed();
        
    }
    
    public void setLEDsOnLeft(ITriColorLEDArray leds, String colour, double freq)
    {
        //ses LEDS on one at a atime startin gwith the left LED
        for(int i=0; i<8; i++)
        {
            //cycle through LEDS assigning to LED instance
            ITriColorLED led = leds.getLED(i);
            
            //wait for 250 mils
            Utils.sleep(250);
            if(colour.equals("red"))
                led.setRGB(255, 120, 60);
            if(colour.equals("green"))
                led.setRGB(120, 0, 90);
            if(colour.equals("blue"))
                led.setRGB(0, 0, 255);
            toneGen.startTone(freq, 30);
            led.setOn();
        }
        
    }
    
    public void setLEDsOnRight(ITriColorLEDArray leds, String colour, double freq)
    {
        //ses LEDS on one at a atime startin gwith the left LED
        for(int i=7; i>-1; i--)
        {
            //cycle through LEDS assigning to LED instance
            ITriColorLED led = leds.getLED(i);
            
            //wait for 250 mils
            Utils.sleep(250);
            if(colour.equals("red"))
                led.setRGB(255, 0, 0);
            if(colour.equals("green"))
                led.setRGB(0, 255, 0);
            if(colour.equals("blue"))
                led.setRGB(0, 0, 255);
            toneGen.startTone(freq, 30);
            led.setOn();
        }
        
    }
    
    public void setLEDsOFFLeft(ITriColorLEDArray leds)
    {
        for(int i=0; i<8; i++)
        {
            ITriColorLED led = leds.getLED(i);            
            Utils.sleep(250);   
            led.setOff();
            //toneGen.startTone(100, 30);
        }
    }
    
    protected void pauseApp()
    {
        //this is not current called by the Squawk VM
    }
    
    //Called if the MIDlet is terminated by the system.
    //it is not caled if MIDlet.notifyDestroyed() was called
    
    protected void destroyApp(boolean unconditional) throws 
            MIDletStateChangeException
    {
    }
}
